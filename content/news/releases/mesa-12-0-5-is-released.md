---
title:    "Mesa 12.0.5 is released"
date:     2016-12-05 00:00:00
category: releases
tags:     []
summary:  "[Mesa 12.0.5](https://docs.mesa3d.org/relnotes/12.0.5.html) is released. This is a bug-fix
release."
---
[Mesa 12.0.5](https://docs.mesa3d.org/relnotes/12.0.5.html) is released. This is a bug-fix
release.

{{< alert type="info" title="Note" >}}
It is anticipated that 12.0.5 will be the final release in the
12.0 series. Users of 12.0 are encouraged to migrate to the 13.0 series
in order to obtain future fixes.
{{< /alert >}}
