---
title:    "Mesa 19.0.3 is released"
date:     2019-04-24 00:00:00
category: releases
tags:     []
---
[Mesa 19.0.3](https://docs.mesa3d.org/relnotes/19.0.3.html) is released. This is a bug-fix
release.
