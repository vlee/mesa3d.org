---
title:    "Mesa 19.3.5 is released"
date:     2020-03-09 00:00:00
category: releases
tags:     []
---
[Mesa 19.3.5](https://docs.mesa3d.org/relnotes/19.3.5.html) is released. This is a bug fix
release, and the final 19.3.x release. Users are encouraged to migrate
to 20.0.x in order to obtain future fixes.
