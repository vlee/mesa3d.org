---
title:    "Mesa 20.0.6 is released"
date:     2020-04-29 00:00:00
category: releases
tags:     []
---
[Mesa 20.0.6](https://docs.mesa3d.org/relnotes/20.0.6.html) is released. This is a bug fix
release.
