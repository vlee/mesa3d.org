---
title:    "Mesa 7.0.2 is released"
date:     2007-11-10 00:00:00
category: releases
tags:     []
---
[Mesa 7.0.2](https://docs.mesa3d.org/relnotes/7.0.2.html) is released. This is a bug-fix
release.
