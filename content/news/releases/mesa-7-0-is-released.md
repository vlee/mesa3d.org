---
title:    "Mesa 7.0 is released"
date:     2007-06-22 00:00:00
category: releases
tags:     []
---
[Mesa 7.0](https://docs.mesa3d.org/relnotes/7.0.html) is released. This is a stable release
featuring OpenGL 2.1 support.
