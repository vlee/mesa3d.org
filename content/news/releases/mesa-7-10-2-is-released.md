---
title:    "Mesa 7.10.2 is released"
date:     2011-04-06 00:00:00
category: releases
tags:     []
---
[Mesa 7.10.2](https://docs.mesa3d.org/relnotes/7.10.2.html) is released. This is a bug fix
release.
