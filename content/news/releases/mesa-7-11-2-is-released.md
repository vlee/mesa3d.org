---
title:    "Mesa 7.11.2 is released"
date:     2011-11-27 00:00:00
category: releases
tags:     []
---
[Mesa 7.11.2](https://docs.mesa3d.org/relnotes/7.11.2.html) is released. This is a bug fix
release. This release was made primarily to fix build problems with
7.11.1 on Mandriva and to fix problems related to glCopyTexImage to
luminance-alpha textures. The later was believed to have been fixed in
7.11.1 but was not.
