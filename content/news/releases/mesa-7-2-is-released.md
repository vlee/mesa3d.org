---
title:    "Mesa 7.2 is released"
date:     2008-09-20 00:00:00
category: releases
tags:     []
---
[Mesa 7.2](https://docs.mesa3d.org/relnotes/7.2.html) is released. This is a stable, bug-fix
release.
