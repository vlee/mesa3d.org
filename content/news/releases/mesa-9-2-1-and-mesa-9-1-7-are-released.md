---
title:    "Mesa 9.2.1 and Mesa 9.1.7 are released"
date:     2013-10-04 00:00:00
category: releases
tags:     []
---
[Mesa 9.2.1](https://docs.mesa3d.org/relnotes/9.2.1.html) and [Mesa
9.1.7](https://docs.mesa3d.org/relnotes/9.1.7.html) are released, both bug-fix releases.
